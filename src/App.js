import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import Count from "./component/count";

function App() {
  return (
    <div className="container text-center mt-5">
      <div className="row justify-content-center">
        <div className="col-6">
        <Count></Count>
        </div>
      </div>
    </div>

  );
}

export default App;
