import { useEffect, useState } from "react"

function Count () {

    const [Count, setCount] = useState(0);

    const onClickMeHandle =()=>{
        console.log("increase count: " + Count)
        setCount(Count+1)
    }

    //chạy mỗi khi render lại: componentDidUpdate
    useEffect(()=>{
        console.log("componentDidUpdate ...");
        document.title = `You clicked ${Count} times!`;
        return()=>{
            console.log("componentWillUnmout")
        };
    },[])

    return (
        <div>
        <p>You clicked {Count} times!</p>
        <button className="btn btn-success" onClick={onClickMeHandle}>Click me!</button>
        </div>
    )
}

export default Count